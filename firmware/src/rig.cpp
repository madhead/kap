#include <Arduino.h>
// #include <PinChangeInterruptHandler.h>
#include <RFReceiver.h>
#include <Servo.h>

#define PIN_SERVO_PITCH 3
#define PIN_SERVO_YAW 2
#define PIN_RECEIVER 4

Servo pitch;
Servo yaw;
RFReceiver receiver(PIN_RECEIVER);
byte data[2];

void setup()
{
    Serial.begin(9600);

    pitch.attach(PIN_SERVO_PITCH);
    yaw.attach(PIN_SERVO_YAW);
}

void loop()
{
    byte msg[2];
    byte senderId = 0;
    byte packageId = 0;
    byte len = receiver.recvPackage((byte *)msg, &senderId, &packageId);

    Serial.println("");
    Serial.print("Package: ");
    Serial.println(packageId);
    Serial.print("Sender: ");
    Serial.println(senderId);
    Serial.print("Pitch: ");
    Serial.println(msg[0]);
    Serial.print("Yaw: ");
    Serial.println(msg[1]);

    // pitch.writeMicroseconds(1400);
    // yaw.writeMicroseconds(1600);

    // Serial.println("loop");
}
