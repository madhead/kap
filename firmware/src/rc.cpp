#include <Arduino.h>
#include <RFTransmitter.h>

#define NODE_ID 1
#define PIN_BUTTON 2
#define PIN_TRANSMITTER 6

byte clicks = 0;
RFTransmitter transmitter(
    /*outputPin*/ PIN_TRANSMITTER,
    /*nodeId*/ NODE_ID,
    /*pulseLength*/ 100,
    /*backoffDelay*/ 100,
    /*resendCount*/ 2
    /**/
);

void click()
{
    static unsigned long lastClickTime = 0;
    unsigned long now = millis();

    if (now - lastClickTime > 200)
    {
        clicks++;
    }

    lastClickTime = now;
}

// cppcheck-suppress unusedFunction
void setup()
{
    Serial.begin(9600);
    pinMode(PIN_BUTTON, INPUT_PULLUP);

    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), click, FALLING);
}

// cppcheck-suppress unusedFunction
void loop()
{
    static int cachedClicks = 0;

    if (cachedClicks != clicks)
    {
        Serial.print("Clicks: ");
        Serial.println(clicks);
        transmitter.send(&clicks, 1);
        cachedClicks = clicks;
    }
}
