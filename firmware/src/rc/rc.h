#ifndef KAP_RC
#define KAP_RC

class Rc
{
private:
    volatile bool _enabled = true;

public:
    void toggle();
    bool enabled();
};

#endif
