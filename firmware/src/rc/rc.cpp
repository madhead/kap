#include "rc.h"

void Rc::toggle()
{
    this->_enabled = !(this->_enabled);
}

bool Rc::enabled()
{
    return this->_enabled;
}
