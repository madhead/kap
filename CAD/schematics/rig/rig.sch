EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Motor:Motor_Servo M2
U 1 1 5DCF00A2
P 6750 3450
F 0 "M2" H 6700 3650 50  0000 L CNN
F 1 "Yaw servo" H 6600 3250 50  0000 L CNN
F 2 "" H 6750 3260 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 6750 3260 50  0001 C CNN
	1    6750 3450
	1    0    0    -1  
$EndComp
$Comp
L Motor:Motor_Servo M1
U 1 1 5DCF0513
P 7750 4100
F 0 "M1" H 7700 4300 50  0000 L CNN
F 1 "Pitch servo" H 7600 3850 50  0000 L CNN
F 2 "" H 7750 3910 50  0001 C CNN
F 3 "http://forums.parallax.com/uploads/attachments/46831/74481.png" H 7750 3910 50  0001 C CNN
	1    7750 4100
	1    0    0    -1  
$EndComp
$Comp
L KAP:RF_Receiver_433_MHz RF1
U 1 1 5DCF0CFA
P 3750 3700
F 0 "RF1" V 3400 4000 60  0000 C CNN
F 1 "XY-MK-5V" V 4100 4050 60  0000 C CNN
F 2 "" H 3850 3700 60  0000 C CNN
F 3 "" H 3850 3700 60  0000 C CNN
	1    3750 3700
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5DCF1490
P 8250 4950
F 0 "#PWR01" H 8250 4700 50  0001 C CNN
F 1 "GND" V 8255 4822 50  0000 R CNN
F 2 "" H 8250 4950 50  0001 C CNN
F 3 "" H 8250 4950 50  0001 C CNN
	1    8250 4950
	0    -1   -1   0   
$EndComp
$Comp
L power:+9V #PWR02
U 1 1 5DCF1D30
P 8250 2500
F 0 "#PWR02" H 8250 2350 50  0001 C CNN
F 1 "+9V" V 8265 2628 50  0000 L CNN
F 2 "" H 8250 2500 50  0001 C CNN
F 3 "" H 8250 2500 50  0001 C CNN
	1    8250 2500
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 4750 5150 4950
Wire Wire Line
	5250 2750 5250 2500
Wire Wire Line
	5150 4950 6150 4950
Wire Wire Line
	7450 4200 6850 4200
Wire Wire Line
	6850 4200 6850 4950
Connection ~ 6850 4950
Wire Wire Line
	6850 4950 7350 4950
Wire Wire Line
	6450 3550 6150 3550
Wire Wire Line
	6150 3550 6150 4950
Connection ~ 6150 4950
Wire Wire Line
	6150 4950 6350 4950
Wire Wire Line
	6450 3350 5650 3350
Wire Wire Line
	7450 4000 6000 4000
Wire Wire Line
	6000 4000 6000 3450
Wire Wire Line
	6000 3450 5650 3450
Wire Wire Line
	3750 3900 4200 3900
Wire Wire Line
	4200 3900 4200 4950
Connection ~ 5150 4950
Wire Wire Line
	3750 3600 4000 3600
Wire Wire Line
	4000 3600 4000 3400
Wire Wire Line
	4950 2500 4950 2600
Wire Wire Line
	3750 3800 4000 3800
Wire Wire Line
	4100 3800 4100 5050
Wire Wire Line
	4100 5050 5850 5050
Wire Wire Line
	5850 5050 5850 3550
Wire Wire Line
	5850 3550 5650 3550
Wire Wire Line
	5250 2500 8250 2500
Wire Wire Line
	7450 4100 7350 4100
Wire Wire Line
	7150 4100 7150 2600
Wire Wire Line
	7150 2600 6150 2600
Connection ~ 4950 2600
Wire Wire Line
	4950 2600 4950 2750
Wire Wire Line
	6450 3450 6350 3450
Wire Wire Line
	6150 3450 6150 2600
Connection ~ 6150 2600
Wire Wire Line
	6150 2600 4950 2600
$Comp
L Device:CP C1
U 1 1 5DD98FAD
P 4200 3650
F 0 "C1" H 4318 3696 50  0000 L CNN
F 1 "100 μF" H 4200 3550 50  0000 L CNN
F 2 "" H 4238 3500 50  0001 C CNN
F 3 "~" H 4200 3650 50  0001 C CNN
	1    4200 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5DD9ECB8
P 6350 4450
F 0 "C2" H 6468 4496 50  0000 L CNN
F 1 "470 μF" H 6468 4405 50  0000 L CNN
F 2 "" H 6388 4300 50  0001 C CNN
F 3 "~" H 6350 4450 50  0001 C CNN
	1    6350 4450
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C3
U 1 1 5DD9F85A
P 7350 4450
F 0 "C3" H 7468 4496 50  0000 L CNN
F 1 "470 μF" H 7468 4405 50  0000 L CNN
F 2 "" H 7388 4300 50  0001 C CNN
F 3 "~" H 7350 4450 50  0001 C CNN
	1    7350 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4300 6350 3450
Connection ~ 6350 3450
Wire Wire Line
	6350 3450 6150 3450
Wire Wire Line
	6350 4600 6350 4950
Connection ~ 6350 4950
Wire Wire Line
	6350 4950 6850 4950
Wire Wire Line
	7350 4100 7350 4300
Connection ~ 7350 4100
Wire Wire Line
	7350 4100 7150 4100
Wire Wire Line
	7350 4600 7350 4950
Connection ~ 7350 4950
Wire Wire Line
	7350 4950 8250 4950
Wire Wire Line
	4000 2500 4950 2500
Wire Wire Line
	4200 4950 5150 4950
Wire Wire Line
	4200 3500 4200 3400
Wire Wire Line
	4200 3400 4000 3400
Connection ~ 4000 3400
Wire Wire Line
	4000 3400 4000 2500
Wire Wire Line
	4200 3800 4200 3900
Connection ~ 4200 3900
Wire Wire Line
	3750 3700 4000 3700
Wire Wire Line
	4000 3700 4000 3800
Connection ~ 4000 3800
Wire Wire Line
	4000 3800 4100 3800
$Comp
L MCU_Module:Arduino_Nano_v3.x A?
U 1 1 5EB9C498
P 5150 3750
F 0 "A?" H 5150 2661 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 5150 2570 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 5150 3750 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5150 3750 50  0001 C CNN
	1    5150 3750
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
